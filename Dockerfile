FROM maven:3.8.5-openjdk-17 as build
WORKDIR /usr/src/app

COPY ./pom.xml /usr/src/app/pom.xml
RUN mvn dependency:go-offline

COPY . /usr/src/app
RUN mvn package

FROM eclipse-temurin:17-jdk

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/target/vertical-logistics-transformer-api-0.0.1-SNAPSHOT.jar app.jar

CMD ["java", "-jar", "app.jar"]
