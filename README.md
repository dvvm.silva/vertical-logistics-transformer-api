
#  LuizaLabs - Desafio técnico - Vertical Logística



### Benefícios

A integração eficiente entre os sistemas proporcionará uma melhor utilização dos dados provenientes do sistema legado.
A normalização para o formato JSON facilitará a interoperabilidade e a análise dos dados por parte de outros sistemas.


#### Observações:

O sucesso desse projeto não apenas atenderá à demanda específica de integração, mas também contribuirá para uma melhor organização e compreensão dos dados, promovendo a eficiência operacional no ambiente empresarial.
### Escolhas utilizadas

#### Refatoração para Métodos Separados

A lógica de parsing foi dividida em vários métodos separados, cada um com uma responsabilidade específica. Isso melhora a legibilidade do código e facilita a manutenção, uma vez que cada método realiza uma tarefa clara.

 **Benefícios:**
Código mais modular, fácil de entender e de dar manutenção.
Os métodos separados facilitam a reutilização de código em outros contextos.

#### Uso de Classes Abstratas
A classe AbstractFileParser fornece uma estrutura comum para as classes que realizam o parsing de arquivos. Ela encapsula a lógica comum e define métodos que podem ser sobrescritos por subclasses.

**Benefícios:**
Promove a reutilização de código e fornece uma estrutura consistente para classes que realizam tarefas semelhantes.

#### Testes Unitários

A implementação de testes unitários garante que cada componente do sistema funcione conforme o esperado. Isso é fundamental para detectar falhas rapidamente e garantir que as futuras alterações não introduzam regressões.

**Benefícios:**
Confiança na robustez do código.
Facilita a detecção precoce de bugs.
Permite alterações no código com mais segurança.

#### Utilização de Mocks para Testes Isolados

Para isolar a lógica de parsing e garantir que os testes se concentrem apenas na unidade em teste, foram utilizados mocks para simular o comportamento de classes externas.

**Benefícios:**
Isolamento de dependências externas, garantindo que os testes se concentrem apenas no código da unidade sendo testada.

#### Tratamento de Exceções Adequado

O código inclui tratamento de exceções adequado para lidar com erros durante a leitura de arquivos, parsing e outras operações que podem lançar exceções.

**Benefícios:**
Melhora a robustez do código.
Fornece um tratamento adequado para situações excepcionais.
Evita que o programa quebre inesperadamente.
## Documentação da API

### Upload de Arquivo e Processamento

Realize o envio de um arquivo para processamento com opções adicionais.


```http
  POST /api/upload
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `file` | `multipart` | **Obrigatório**. O arquivo a ser enviado.|
| `userId` | `integer` | **(Opcional)**. ID do usuário para filtragem.|
| `file` | `multipart` | **(Opcional)**. Data de início para filtrar (DD/MM/YYYY).|
| `file` | `multipart` | **(Opcional)**. Data de término para filtrar (DD/MM/YYYY).

### Respostas Possíveis
**200 OK:** A solicitação foi processada com sucesso, e a resposta contém os itens processados.

**400 Bad Request:** A solicitação está faltando a chave da API.

**500 Internal Server Error:** Ocorreu um erro durante a recuperação dos itens processados.

# Executando o Projeto com Docker Compose

### Pré-requisitos

Docker instalado na máquina: https://docs.docker.com/get-docker/

### Passos para Execução
#### 1° Clone o Repositório:

git clone https://gitlab.com/dvvm.silva/vertical-logistics-transformer-api.git

#### 2° Construa a Imagem Docker:

docker-compose build

#### 3° Construa a Imagem Docker:

docker-compose up -d

### Acesse o Aplicativo:

O aplicativo estará disponível em http://localhost:3000.

O swagger estará disponível em http://localhost:3000/swagger-ui/index.html

## Observações Adicionais
Certifique-se de que a porta 3000 esteja disponível em sua máquina para acessar o aplicativo.

Para visualizar os logs do contêiner em tempo real, utilize o seguinte comando:

**bash Copy code docker-compose logs -f**

Caso ocorram problemas durante a execução, verifique os logs para diagnosticar possíveis falhas.

Com esses passos, você deverá ter o seu projeto Dockerizado em execução 