package com.luizaLabs.verticallogisticstransformerapi.config;


import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI api() {
        return new OpenAPI().info(new Info()
                .title("LuizaLabs - Desafio técnico - Vertical Logística")
                .contact(new Contact().name("Davisson Silva")
                        .email("davisson.moc@gmail.com")
                        .url("https://www.linkedin.com/in/davisson-silva-09a55a1a8/"))
                .description("recebe um arquivo via API REST e processe-o para ser retornado via API REST."));
    }
}
