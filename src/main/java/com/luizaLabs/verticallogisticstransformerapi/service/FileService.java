package com.luizaLabs.verticallogisticstransformerapi.service;

import com.luizaLabs.verticallogisticstransformerapi.parsers.AbstractFileParser;
import com.luizaLabs.verticallogisticstransformerapi.response.UserResponse;
import com.luizaLabs.verticallogisticstransformerapi.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class FileService extends AbstractFileParser {

    public List<UserResponse> processFile(MultipartFile file, Integer userId, String startDate, String endDate) {
        List<UserResponse> userResponses = readLinesFromFile(file);

        if (Objects.nonNull(userId)) {
            userResponses = filterByUserId(userResponses, userId);
        }

        if (Objects.nonNull(startDate) && Objects.nonNull(endDate)) {
            userResponses = filterByDateRange(userResponses, startDate, endDate);
        }

        return userResponses;
    }

    private List<UserResponse> readLinesFromFile(MultipartFile file) {
        List<UserResponse> userResponses = new ArrayList<>();

        try (InputStream inputStream = file.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {
                UserResponse userResponse = parseLine(line);
                userResponses.add(userResponse);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return userResponses;
    }

    public List<UserResponse> filterByUserId(List<UserResponse> userResponses, int userId) {
        return userResponses.stream()
                .filter(userResponse -> userResponse.getUserId() == userId)
                .toList();
    }

    public List<UserResponse> filterByDateRange(List<UserResponse> userResponses,
                                                       String startDateStr, String endDateStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date startDate;
        Date endDate;
        try {
            startDate = dateFormat.parse(startDateStr);
            endDate = dateFormat.parse(endDateStr);
        } catch (ParseException e) {
            throw new RuntimeException("Erro ao converter a data.", e);
        }

        return userResponses.stream()
                .filter(userResponse ->
                        userResponse.getOrders().stream()
                                .anyMatch(order ->
                                        DateUtils.isDateInRange(order.getDate(), startDate, endDate)))
                .toList();
    }
}
