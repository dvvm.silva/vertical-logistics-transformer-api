package com.luizaLabs.verticallogisticstransformerapi.utils;

import lombok.experimental.UtilityClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@UtilityClass
public class DateUtils {

    private static final String DATE_FORMAT = "yyyyMMdd";

    public Date parseDate(String dateStr) {
        try {
            return new SimpleDateFormat(DATE_FORMAT).parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException("Erro ao converter a data.", e);
        }
    }

    public boolean isDateInRange(String dateString, Date startDate, Date endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = dateFormat.parse(dateString);
            return date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0;
        } catch (ParseException e) {
            throw new RuntimeException("Erro ao converter a data.", e);
        }
    }

}
