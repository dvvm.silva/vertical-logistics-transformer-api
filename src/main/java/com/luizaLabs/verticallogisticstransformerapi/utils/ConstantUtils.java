package com.luizaLabs.verticallogisticstransformerapi.utils;

public abstract class ConstantUtils {
    public static final int USER_ID_START = 0;
    public static final int USER_ID_END = 10;
    public static final int USER_NAME_START = 10;
    public static final int USER_NAME_END = 55;
    public static final int ORDER_ID_START = 55;
    public static final int ORDER_ID_END = 65;
    public static final int PRODUCT_ID_START = 65;
    public static final int PRODUCT_ID_END = 75;
    public static final int VALUE_START = 75;
    public static final int VALUE_END = 87;
    public static final int DATE_START = 87;
    public static final int DATE_END = 95;
    public static final String FORMATO_DESEJADO = "dd/MM/yyyy";
}
