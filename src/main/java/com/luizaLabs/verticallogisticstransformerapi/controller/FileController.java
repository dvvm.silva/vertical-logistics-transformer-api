package com.luizaLabs.verticallogisticstransformerapi.controller;

import com.luizaLabs.verticallogisticstransformerapi.response.UserResponse;
import com.luizaLabs.verticallogisticstransformerapi.service.FileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FileController {

    @Autowired
    private FileService fileService;
    @Operation(summary = "Upload de arquivo e processamento",
            description = "Envia um arquivo para processamento com opções adicionais.")
    @PostMapping("/upload")
    public ResponseEntity<List<UserResponse>> handleFileUpload(
            @Parameter(description = "O arquivo a ser enviado") @RequestPart(value = "file") MultipartFile file,
            @Parameter(description = "ID do usuário (opcional)") @RequestParam(required = false) Integer userId,
            @Parameter(description = "Data de início para filtrar (opcional) no formato DD/MM/YYYY") @RequestParam(required = false) String startDate,
            @Parameter(description = "Data de término para filtrar (opcional) no formato DD/MM/YYYY") @RequestParam(required = false) String endDate) {
        return ResponseEntity.ok(this.fileService.processFile(file, userId, startDate, endDate));
    }
}
