package com.luizaLabs.verticallogisticstransformerapi.response;

import lombok.Data;

@Data
public class ProductResponse {
    private int productId;
    private String value;
}
