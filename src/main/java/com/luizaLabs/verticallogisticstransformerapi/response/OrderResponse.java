package com.luizaLabs.verticallogisticstransformerapi.response;

import lombok.Data;

import java.util.List;

@Data
public class OrderResponse {
    private int orderId;
    private String total;
    private String date;
    private List<ProductResponse> products;
}
