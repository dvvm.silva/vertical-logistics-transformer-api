package com.luizaLabs.verticallogisticstransformerapi.parsers;

import com.luizaLabs.verticallogisticstransformerapi.response.OrderResponse;
import com.luizaLabs.verticallogisticstransformerapi.response.ProductResponse;
import com.luizaLabs.verticallogisticstransformerapi.response.UserResponse;
import com.luizaLabs.verticallogisticstransformerapi.utils.ConstantUtils;
import com.luizaLabs.verticallogisticstransformerapi.utils.DateUtils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;

public abstract class AbstractFileParser {

    public UserResponse parseLine(String line) {
        UserResponse userResponse = new UserResponse();

        BigDecimal value = parseValue(line);

        userResponse.setUserId(parseUserId(line));
        userResponse.setName(parseUserName(line));

        OrderResponse orderResponse = createOrderResponse(line, value, parseDate(line));
        ProductResponse productResponse = createProductResponse(line, value);

        orderResponse.setProducts(Collections.singletonList(productResponse));
        userResponse.setOrders(Collections.singletonList(orderResponse));

        return userResponse;
    }

    private BigDecimal parseValue(String line) {
        return new BigDecimal(line.substring(ConstantUtils.VALUE_START, ConstantUtils.VALUE_END).trim());
    }

    private String parseDate(String line) {
        DateFormat dateFormat = new SimpleDateFormat(ConstantUtils.FORMATO_DESEJADO);
        return dateFormat.format(DateUtils.parseDate(line.substring(ConstantUtils.DATE_START, ConstantUtils.DATE_END).trim()));
    }

    private int parseUserId(String line) {
        return Integer.parseInt(line.substring(ConstantUtils.USER_ID_START, ConstantUtils.USER_ID_END).trim());
    }

    private String parseUserName(String line) {
        return line.substring(ConstantUtils.USER_NAME_START, ConstantUtils.USER_NAME_END).trim();
    }

    private OrderResponse createOrderResponse(String line, BigDecimal value, String date) {
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setOrderId(parseOrderId(line));
        orderResponse.setTotal(String.valueOf(value));
        orderResponse.setDate(String.valueOf(date));
        return orderResponse;
    }

    private int parseOrderId(String line) {
        return Integer.parseInt(line.substring(ConstantUtils.ORDER_ID_START, ConstantUtils.ORDER_ID_END).trim());
    }

    private ProductResponse createProductResponse(String line, BigDecimal value) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(parseProductId(line));
        productResponse.setValue(String.valueOf(value));
        return productResponse;
    }

    private int parseProductId(String line) {
        return Integer.parseInt(line.substring(ConstantUtils.PRODUCT_ID_START, ConstantUtils.PRODUCT_ID_END).trim());
    }

}
