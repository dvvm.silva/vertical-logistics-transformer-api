package com.luizaLabs.verticallogisticstransformerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class VerticalLogisticsTransformerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerticalLogisticsTransformerApiApplication.class, args);
	}

}
