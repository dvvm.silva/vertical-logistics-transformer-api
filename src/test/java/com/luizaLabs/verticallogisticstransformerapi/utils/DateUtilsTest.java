package com.luizaLabs.verticallogisticstransformerapi.utils;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DateUtilsTest {

    private static final String DATE_FORMAT = "yyyyMMdd";


    @Test
    public void testParseDateSuccessfully() throws ParseException {
        String dateStr = "20240120";
        Date result = DateUtils.parseDate(dateStr);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date expectedDate = sdf.parse(dateStr);
        assertThat(result).isEqualTo(expectedDate);
    }

    @Test
    public void testParseDateWithInvalidFormat() {
        String invalidDateStr = "02/08/2021";
        assertThrows(RuntimeException.class, () -> DateUtils.parseDate(invalidDateStr));
    }

    @Test
    public void testIsDateInRange() throws ParseException {
        // Data de teste no formato dd/MM/yyyy
        String dateString = "10/01/2024";

        // Mock das datas de início e fim
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2024");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("31/01/2024");

        // Chama o método a ser testado
        boolean result = DateUtils.isDateInRange(dateString, startDate, endDate);

        // Verifica se a data está dentro do intervalo esperado
        assertTrue(result);
    }

    @Test
    public void testIsDateNotInRange() throws ParseException {
        String dateString = "15/02/2024";
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2024");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("31/01/2024");

        boolean result = DateUtils.isDateInRange(dateString, startDate, endDate);

        assertFalse(result);
    }

    @Test
    public void testIsDateInRangeWithParseException() throws ParseException {
        String invalidDateString = "2024-01-10";

        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2024");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("31/01/2024");

        assertThrows(RuntimeException.class, () -> DateUtils.isDateInRange(invalidDateString, startDate, endDate));
    }

}