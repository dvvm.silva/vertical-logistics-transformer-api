package com.luizaLabs.verticallogisticstransformerapi.service;

import com.luizaLabs.verticallogisticstransformerapi.response.OrderResponse;
import com.luizaLabs.verticallogisticstransformerapi.response.UserResponse;
import net.bytebuddy.dynamic.loading.ClassFilePostProcessor;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@SpringBootTest
class FileServiceTest {

    @Mock
    private ClassFilePostProcessor fileProcessor;

    @InjectMocks
    private FileService fileReaderService; // Você precisa substituir FileReaderService pelo nome real do serviço usado para ler linhas do arquivo.

    @Test
    void testProcessFileWithUserIdAndDateRange () {

        MockMultipartFile mockFile = new MockMultipartFile("file", "test.txt", "text/plain", "0000000070                              Palmer Prosacco00000007530000000003     1836.7420210308".getBytes());

        Integer userId = 3;
        String startDate = "02/08/2021";
        String endDate = "04/11/2021";

        List<OrderResponse> orders = new ArrayList<>();

        List<UserResponse> mockUserResponses = Arrays.asList(
                new UserResponse(1, "John Doe", orders),
                new UserResponse(2, "Jane Doe", orders)
        );

        fileReaderService.processFile(mockFile, userId, startDate, endDate);
    }
}