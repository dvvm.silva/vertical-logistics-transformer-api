package com.luizaLabs.verticallogisticstransformerapi.controller;

import com.luizaLabs.verticallogisticstransformerapi.response.UserResponse;
import com.luizaLabs.verticallogisticstransformerapi.service.FileService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class FileControllerTest {

    @Mock
    private FileService fileService;

    @InjectMocks
    private FileController fileController;

    @Test
    void handleFileUpload_Success() {
        MockMultipartFile mockFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "file content".getBytes());

        List<UserResponse> mockUserResponses = List.of(new UserResponse());

        when(fileService.processFile(any(), any(), any(), any())).thenReturn(mockUserResponses);


        ResponseEntity<List<UserResponse>> responseEntity = fileController.handleFileUpload(mockFile, 1,
                "20220101", "20220131");

        verify(fileService, times(1)).processFile(mockFile, 1, "20220101",
                "20220131");

        verifyNoMoreInteractions(fileService);

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertSame(mockUserResponses, responseEntity.getBody())
        );
    }
}